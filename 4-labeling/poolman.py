from modelbase.ode import Model


def ADP(ATP: float, AP_total: float) -> float:
    return AP_total - ATP


def P_i(
    PGA: float,
    BPGA: float,
    GAP: float,
    DHAP: float,
    FBP: float,
    F6P: float,
    G6P: float,
    G1P: float,
    SBP: float,
    S7P: float,
    E4P: float,
    X5P: float,
    R5P: float,
    RUBP: float,
    RU5P: float,
    ATP: float,
    phosphate_total: float,
) -> float:
    return phosphate_total - (
        PGA
        + 2 * BPGA
        + GAP
        + DHAP
        + 2 * FBP
        + F6P
        + G6P
        + G1P
        + 2 * SBP
        + S7P
        + E4P
        + X5P
        + R5P
        + 2 * RUBP
        + RU5P
        + ATP
    )


def N(
    P_pool: float,
    PGA: float,
    GAP: float,
    DHAP: float,
    Kpxt: float,
    Pext: float,
    Kpi: float,
    Kpga: float,
    Kgap: float,
    Kdhap: float,
) -> float:
    return 1 + (1 + (Kpxt / Pext)) * (
        (P_pool / Kpi) + (PGA / Kpga) + (GAP / Kgap) + (DHAP / Kdhap)
    )


def v1(
    RUBP: float,
    PGA: float,
    FBP: float,
    SBP: float,
    P_pool: float,
    V1: float,
    Km1: float,
    Ki11: float,
    Ki12: float,
    Ki13: float,
    Ki14: float,
    Ki15: float,
    NADPH_pool: float,
) -> float:
    return (V1 * RUBP) / (
        RUBP
        + Km1
        * (
            1
            + (PGA / Ki11)
            + (FBP / Ki12)
            + (SBP / Ki13)
            + (P_pool / Ki14)
            + (NADPH_pool / Ki15)
        )
    )


def v6(
    FBP: float,
    F6P: float,
    P: float,
    V6: float,
    Km6: float,
    Ki61: float,
    Ki62: float,
) -> float:
    return (V6 * FBP) / (FBP + Km6 * (1 + (F6P / Ki61) + (P / Ki62)))


def v9(
    SBP: float,
    P: float,
    V9: float,
    Km9: float,
    Ki9: float,
) -> float:
    return (V9 * SBP) / (SBP + Km9 * (1 + (P / Ki9)))


def v13(
    RU5P: float,
    ATP: float,
    PGA: float,
    RUBP: float,
    ADP: float,
    P_pool: float,
    V13: float,
    Km131: float,
    Km132: float,
    Ki131: float,
    Ki132: float,
    Ki133: float,
    Ki134: float,
    Ki135: float,
) -> float:
    return (V13 * RU5P * ATP) / (
        (RU5P + Km131 * (1 + (PGA / Ki131) + (RUBP / Ki132) + (P_pool / Ki133)))
        * (ATP * (1 + (ADP / Ki134)) + Km132 * (1 + (ADP / Ki135)))
    )


def v16(
    ADP: float,
    Phosphate_i: float,
    V16: float,
    Km161: float,
    Km162: float,
) -> float:
    return (V16 * ADP * Phosphate_i) / ((ADP + Km161) * (Phosphate_i + Km162))


def vStarchProduction(
    G1P: float,
    ATP: float,
    PGA: float,
    F6P: float,
    FBP: float,
    ADP: float,
    P_pool: float,
    Vst: float,
    Kmst1: float,
    Kmst2: float,
    Kist: float,
    Kast1: float,
    Kast2: float,
    Kast3: float,
) -> float:
    return (Vst * G1P * ATP) / (
        (G1P + Kmst1)
        * (
            (1 + (ADP / Kist)) * (ATP + Kmst2)
            + ((Kmst2 * P_pool) / (Kast1 * PGA + Kast2 * F6P + Kast3 * FBP))
        )
    )


def v_out(
    s1: float,
    N_total: float,
    VMax_efflux: float,
    K_efflux: float,
) -> float:
    return (VMax_efflux * s1) / (N_total * K_efflux)


def mass_action_1(
    S1: float,
    kf: float,
) -> float:
    return kf * S1


def mass_action_2(
    S1: float,
    S2: float,
    kf: float,
) -> float:
    return kf * S1 * S2


def v3f(
    BPGA: float,
    k3f: float,
    NADPH_pool: float,
    proton_pool_stroma: float,
) -> float:
    return k3f * NADPH_pool * BPGA * proton_pool_stroma


def v3r(
    GAP: float,
    P_i: float,
    k3r: float,
    NADP_pool: float,
) -> float:
    return k3r * GAP * NADP_pool * P_i


def get_model() -> Model:
    m = Model()
    m.add_parameters(
        {
            "Km_1": 0.02,  # [mM], Pettersson 1988
            "Km_6": 0.03,  # [mM], Pettersson 1988
            "Km_9": 0.013,  # [mM], Pettersson 1988
            "Km_13_1": 0.05,  # [mM], Pettersson 1988
            "Km_13_2": 0.05,  # [mM], Pettersson 1988
            "Km_16_1": 0.014,  # [mM], Pettersson 1988
            "Km_16_2": 0.3,  # [mM], Pettersson 1988
            "Km_starch_1": 0.08,  # [mM], Pettersson 1988
            "Km_starch_2": 0.08,  # [mM], Pettersson 1988
            "K_pga": 0.25,  # [mM], Pettersson 1988
            "K_gap": 0.075,  # [mM], Pettersson 1988
            "K_dhap": 0.077,  # [mM], Pettersson 1988
            "K_pi": 0.63,  # [mM], Pettersson 1988
            "K_pxt": 0.74,  # [mM], Pettersson 1988
            "Ki_1_1": 0.04,  # [mM], Pettersson 1988
            "Ki_1_2": 0.04,  # [mM], Pettersson 1988
            "Ki_1_3": 0.075,  # [mM], Pettersson 1988
            "Ki_1_4": 0.9,  # [mM], Pettersson 1988
            "Ki_1_5": 0.07,  # [mM], Pettersson 1988
            "Ki_6_1": 0.7,  # [mM], Pettersson 1988
            "Ki_6_2": 12.0,  # [mM], Pettersson 1988
            "Ki_9": 12.0,  # [mM], Pettersson 1988
            "Ki_13_1": 2.0,  # [mM], Pettersson 1988
            "Ki_13_2": 0.7,  # [mM], Pettersson 1988
            "Ki_13_3": 4.0,  # [mM], Pettersson 1988
            "Ki_13_4": 2.5,  # [mM], Pettersson 1988
            "Ki_13_5": 0.4,  # [mM], Pettersson 1988
            "Ki_starch": 10.0,  # [mM], Pettersson 1988
            "Ka_starch_1": 0.1,  # [mM], Pettersson 1988
            "Ka_starch_2": 0.02,  # [mM], Pettersson 1988
            "Ka_starch_3": 0.02,  # [mM], Pettersson 1988
            "CO2": 0.2,  # [mM], Pettersson 1988
            "Phosphate_total": 15.0,  # [mM], Pettersson 1988
            "P_pool_ext": 0.5,  # [mM], Pettersson 1988
            "AP_total": 0.5,  # [mM], Pettersson 1988
            "N_total": 0.5,  # [mM], Pettersson 1988
            "pH_medium": 7.6,  # [], Pettersson 1988
            "pH_stroma": 7.9,  # [], Pettersson 1988
            "proton_pool_stroma": 1.2589254117941661e-05,  # [mM], Pettersson 1988
            "NADPH_pool": 0.21,  # [mM], Pettersson 1988
            "NADP_pool": 0.29,  # [mM], Pettersson 1988
            "k2f": 9.201751269188145,
            "k2r": 32582.211592466298,
            "k3f": 544637974.9812177,
            "k3r": 6.770073507981515e-05,
            "k4f": 71.12668763739163,
            "k4r": 2.536954631987327,
            "k5f": 26.59188391761439,
            "k5r": 2.857113867555865,
            "k7f": 19.397240851045034,
            "k7r": 171.32156056378304,
            "k8f": 70.05167257677905,
            "k8r": 5.610915648763027,
            "k10f": 35.747464921294416,
            "k10r": 37.35248352641369,
            "k11f": 48.64771237135582,
            "k11r": 116.41666392881288,
            "k12f": 157.52592237170072,
            "k12r": 232.8333228289382,
            "k14f": 0.049555583505605835,
            "k14r": 0.021677578735797107,
            "k15f": 0.014301375975283213,
            "k15r": 1.3767755716272655e-10,
            "Vmax_1": 1.1470205806062528,
            "Vmax_6": 0.4549249003859781,
            "Vmax_9": 0.30125280912820146,
            "Vmax_13": 24.755929769008766,
            "Vmax_16": 2.6270790430755477,
            "Vmax_starch": 0.1223590855373052,
            "Vmax_efflux_PGA": 1.1066699104291686,
            "Vmax_efflux_GAP": 0.8479624058609916,
            "Vmax_efflux_DHAP": 0.9327585789768257,
        }
    )
    m.add_compounds(
        [
            "ATP",
            "PGA",
            "BPGA",
            "GAP",
            "DHAP",
            "FBP",
            "F6P",
            "E4P",
            "G6P",
            "G1P",
            "SBP",
            "S7P",
            "X5P",
            "R5P",
            "RU5P",
            "RUBP",
        ]
    )

    m.add_algebraic_module(
        module_name="ADP_mod",
        function=ADP,
        compounds=["ATP"],
        derived_compounds=["ADP"],
        parameters=["AP_total"],
    )
    m.add_algebraic_module(
        module_name="Pi_mod",
        function=P_i,
        compounds=[
            "PGA",
            "BPGA",
            "GAP",
            "DHAP",
            "FBP",
            "F6P",
            "G6P",
            "G1P",
            "SBP",
            "S7P",
            "E4P",
            "X5P",
            "R5P",
            "RUBP",
            "RU5P",
            "ATP",
        ],
        derived_compounds=["P_pool"],
        parameters=["Phosphate_total"],
    )
    m.add_algebraic_module(
        module_name="N_mod",
        function=N,
        compounds=[
            "P_pool",
            "PGA",
            "GAP",
            "DHAP",
        ],
        derived_compounds=["N_pool"],
        parameters=[
            "K_pxt",
            "P_pool_ext",
            "K_pi",
            "K_pga",
            "K_gap",
            "K_dhap",
        ],
    )

    m.add_reaction(
        rate_name="v2f",
        function=mass_action_2,
        stoichiometry={"PGA": -1, "ATP": -1, "BPGA": 1},
        parameters=["k2f"],
    )

    m.add_reaction(
        rate_name="v2r",
        function=mass_action_2,
        stoichiometry={"BPGA": -1, "PGA": 1, "ATP": 1},
        modifiers=["ADP"],
        parameters=["k2r"],
    )

    m.add_reaction(
        rate_name="v3f",
        function=v3f,
        stoichiometry={"BPGA": -1, "GAP": 1},
        parameters=["k3f", "NADPH_pool", "proton_pool_stroma"],
    )

    m.add_reaction(
        rate_name="v3r",
        function=v3r,
        stoichiometry={"GAP": -1, "BPGA": 1},
        modifiers=["P_pool"],
        parameters=["k3r", "NADP_pool"],
    )

    m.add_reaction(
        rate_name="v4f",
        function=mass_action_1,
        stoichiometry={"GAP": -1, "DHAP": 1},
        parameters=["k4f"],
    )

    m.add_reaction(
        rate_name="v4r",
        function=mass_action_1,
        stoichiometry={"DHAP": -1, "GAP": 1},
        parameters=["k4r"],
    )

    m.add_reaction(
        rate_name="v5f",
        function=mass_action_2,
        stoichiometry={"DHAP": -1, "GAP": -1, "FBP": 1},
        parameters=["k5f"],
    )

    m.add_reaction(
        rate_name="v5r",
        function=mass_action_1,
        stoichiometry={"FBP": -1, "DHAP": 1, "GAP": 1},
        parameters=["k5r"],
    )

    m.add_reaction(
        rate_name="v7f",
        function=mass_action_2,
        stoichiometry={"F6P": -1, "GAP": -1, "X5P": 1, "E4P": 1},
        parameters=["k7f"],
    )

    m.add_reaction(
        rate_name="v7r",
        function=mass_action_2,
        stoichiometry={"X5P": -1, "E4P": -1, "F6P": 1, "GAP": 1},
        parameters=["k7r"],
    )

    m.add_reaction(
        rate_name="v8f",
        function=mass_action_2,
        stoichiometry={"DHAP": -1, "E4P": -1, "SBP": 1},
        parameters=["k8f"],
    )

    m.add_reaction(
        rate_name="v8r",
        function=mass_action_1,
        stoichiometry={"SBP": -1, "DHAP": 1, "E4P": 1},
        parameters=["k8r"],
    )

    m.add_reaction(
        rate_name="v10f",
        function=mass_action_2,
        stoichiometry={"S7P": -1, "GAP": -1, "X5P": 1, "R5P": 1},
        parameters=["k10f"],
    )

    m.add_reaction(
        rate_name="v10r",
        function=mass_action_2,
        stoichiometry={"X5P": -1, "R5P": -1, "S7P": 1, "GAP": 1},
        parameters=["k10r"],
    )

    m.add_reaction(
        rate_name="v11f",
        function=mass_action_1,
        stoichiometry={"R5P": -1, "RU5P": 1},
        parameters=["k11f"],
    )

    m.add_reaction(
        rate_name="v11r",
        function=mass_action_1,
        stoichiometry={"RU5P": -1, "R5P": 1},
        parameters=["k11r"],
    )

    m.add_reaction(
        rate_name="v12f",
        function=mass_action_1,
        stoichiometry={"X5P": -1, "RU5P": 1},
        parameters=["k12f"],
    )

    m.add_reaction(
        rate_name="v12r",
        function=mass_action_1,
        stoichiometry={"RU5P": -1, "X5P": 1},
        parameters=["k12r"],
    )

    m.add_reaction(
        rate_name="v14f",
        function=mass_action_1,
        stoichiometry={"F6P": -1, "G6P": 1},
        parameters=["k14f"],
    )

    m.add_reaction(
        rate_name="v14r",
        function=mass_action_1,
        stoichiometry={"G6P": -1, "F6P": 1},
        parameters=["k14r"],
    )

    m.add_reaction(
        rate_name="v15f",
        function=mass_action_1,
        stoichiometry={"G6P": -1, "G1P": 1},
        parameters=["k15f"],
    )

    m.add_reaction(
        rate_name="v15r",
        function=mass_action_1,
        stoichiometry={"G1P": -1, "G6P": 1},
        parameters=["k15r"],
    )

    m.add_reaction(
        rate_name="v1",
        function=v1,
        stoichiometry={"RUBP": -1, "PGA": 2},
        modifiers=[
            "PGA",
            "FBP",
            "SBP",
            "P_pool",
        ],
        parameters=[
            "Vmax_1",
            "Km_1",
            "Ki_1_1",
            "Ki_1_2",
            "Ki_1_3",
            "Ki_1_4",
            "Ki_1_5",
            "NADPH_pool",
        ],
    )

    m.add_reaction(
        rate_name="v6",
        function=v6,
        stoichiometry={"FBP": -1, "F6P": 1},
        modifiers=["F6P", "P_pool"],
        parameters=["Vmax_6", "Km_6", "Ki_6_1", "Ki_6_2"],
    )

    m.add_reaction(
        rate_name="v9",
        function=v9,
        stoichiometry={"SBP": -1, "S7P": 1},
        modifiers=["P_pool"],
        parameters=["Vmax_9", "Km_9", "Ki_9"],
    )

    m.add_reaction(
        rate_name="v13",
        function=v13,
        stoichiometry={"RU5P": -1, "ATP": -1, "RUBP": 1},
        modifiers=[
            "PGA",
            "RUBP",
            "ADP",
            "P_pool",
        ],
        parameters=[
            "Vmax_13",
            "Km_13_1",
            "Km_13_2",
            "Ki_13_1",
            "Ki_13_2",
            "Ki_13_3",
            "Ki_13_4",
            "Ki_13_5",
        ],
    )

    m.add_reaction(
        rate_name="v16",
        function=v16,
        stoichiometry={"ATP": 1},
        modifiers=["ADP", "P_pool"],
        parameters=["Vmax_16", "Km_16_1", "Km_16_2"],
    )

    m.add_reaction(
        rate_name="vSt",
        function=vStarchProduction,
        stoichiometry={"G1P": -1, "ATP": -1},
        modifiers=["PGA", "F6P", "FBP", "ADP", "P_pool"],
        parameters=[
            "Vmax_starch",
            "Km_starch_1",
            "Km_starch_2",
            "Ki_starch",
            "Ka_starch_1",
            "Ka_starch_2",
            "Ka_starch_3",
        ],
    )

    m.add_reaction(
        rate_name="vPGA_out",
        function=v_out,
        stoichiometry={"PGA": -1},
        modifiers=["N_pool"],
        parameters=["Vmax_efflux_PGA", "K_pga"],
    )
    m.add_reaction(
        rate_name="vGAP_out",
        function=v_out,
        stoichiometry={"GAP": -1},
        modifiers=["N_pool"],
        parameters=["Vmax_efflux_GAP", "K_gap"],
    )
    m.add_reaction(
        rate_name="vDHAP_out",
        function=v_out,
        stoichiometry={"DHAP": -1},
        modifiers=["N_pool"],
        parameters=["Vmax_efflux_DHAP", "K_dhap"],
    )
    return m
