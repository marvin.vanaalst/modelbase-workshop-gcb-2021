# modelbase Workshop for German Conference on Bioinformatics 2021

## How to build and analyse mathematical models of biological systems using Python & ​modelbase

Thursday, September 09, 2021, 2 pm – 5 pm

Held by Marvin van Aalst and Anna Matuszyńska, Heinrich Heine University Düsseldorf

Computational mathematical models of biological and biomedical systems have been successfully applied to advance our understanding of various regulatory processes, metabolic fluxes, effects of drug therapies and disease evolution or transmission. Many computational approaches have been developed to support model construction and analysis. We have developed modelbase, an open-source toolbox, to facilitate synergies within the emerging research fields of systems biology and medicine making the overall process of model construction more consistent, understandable, transparent and reproducible.

During this workshop, we will show how to use the software by building a toy model of glycolysis and analysing its dynamic behaviour. We will start with a short introduction to differential equation-based kinetic modelling, during which we will give guidelines to when they should be used and when other techniques are more applicable. We will also demonstrate key features of modelbase using a previously developed model of carbon fixation. During the second part we will provide an interactive learning experience where you will perform an in silico labelling experiment on the glycolysis pathway and investigate how drugs cure cancer, playing with a simple pharmacodynamic model.

Considering the increasing use of Python by computational biologists, a fully embedded modelling package like modelbase is desired and we are happy to present it to you.

**Requirements:**

1. basic knowledge of Python
2. basic knowledge of numpy, pandas and matplotlib

**Resources:**

1. [modelbase software](https://gitlab.com/qtb-hhu/modelbase-software)
2. [Documentation](https://modelbase.readthedocs.io/en/latest/)

## Installation

There are multiple ways you can install modelbase and jupyter notebook for this workshop. They are ordered by ease, but that depends on your pre-existing setup.

### vscode devcontainer

If you happen to already use vscode and docker, the easiest way is to just use the supplied devcontainer in this repository.

```shell
jupyter notebook notebooks
```

### Docker

```shell
docker build --tag modelbase .devcontainer/
docker run -it --user 1000 --publish 8888:8888 --volume `pwd`:/home/vscode/notebooks --workdir "/home/vscode/notebooks" modelbase
jupyter notebook --ip 0.0.0.0 --no-browser
# Open the http://127.0.0.1:8888/?token=.... link that is displayed in browser
```

### conda

If you are using conda, you can easily install modelbase and jupyter notebook from conda-forge. I'd suggest to create a new environment first though, to keep your base environment clean

```python
conda config --add channels conda-forge
conda create --name mb python=3.9
conda activate mb
conda install -c conda-forge modelbase notebook
```

### pip

If you have python >= 3.7 installed on your system, you can just install modelbase and jupyter notebook via pip. I'd suggest to create a virtual environment of you choice first.

`pip install modelbase notebook`

### Environment management

If your system python is < 3.7, things will get a bit hairy though, depending on your operating system. The easiest way here would be to install [miniconda](https://docs.conda.io/en/latest/miniconda.html) and go from there on.
If you dislike conda, you can use [asdf](http://asdf-vm.com/) or [pyenv](https://github.com/pyenv/pyenv-installer) and [pipenv](https://pipenv.kennethreitz.org/en/latest/basics/#environment-management-with-pipenv) together to create an environment with python >= 3.7, but that might take a while. Note that during the workshop we cannot give support for this last option, so you should be reasonably comfortable with the python environment yourself if you want to do this.
